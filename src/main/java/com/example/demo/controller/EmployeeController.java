package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.entity.Employee;

@Controller
@RequestMapping("/employees")
public class EmployeeController{
	
	private List<Employee> EmployeeList;
	
	@PostConstruct
	public void loadList() {
		
		Employee emp1 = new Employee(1,"Yeemon","Kyaw","yeemon@gmail.com");
		Employee emp2 = new Employee(2,"NaingThu","Aung","naingthu@gmail.com");
		
		EmployeeList = new ArrayList<>();
		
		EmployeeList.add(emp1);
		EmployeeList.add(emp2);
	}
	
	@GetMapping("/list")
	public String listEmployee(Model theModel) {
		
		theModel.addAttribute("employees",EmployeeList);
		return "empList";
	}

}
