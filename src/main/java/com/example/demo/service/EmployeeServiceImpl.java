package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.EmployeeRepository;
import com.example.demo.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	private EmployeeRepository theEmployeeRepository;
	
	@Autowired
	public EmployeeServiceImpl(EmployeeRepository enpRepository) {
		theEmployeeRepository = enpRepository;
	}
	
	@Override
	public List<Employee> findAll() {
		return theEmployeeRepository.findAll();
	}

	@Override
	public void save(Employee theEmployee) {
		theEmployeeRepository.save(theEmployee);
	}

	@Override
	public void deleteById(int theId) {
		theEmployeeRepository.deleteById(theId);
	}

	@Override
	public Employee findById(int theId) {
		return theEmployeeRepository.findById(theId);
	}

}
